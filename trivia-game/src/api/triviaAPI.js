export const TriviaAPI = {
  async getCategory() {
    try {
      const { trivia_categories } = await fetch(
        "https://opentdb.com/api_category.php"
      ).then((r) => r.json());
      return [null, trivia_categories];
    } catch (e) {
      return [e.message, []];
    }
  },

  async getTriviaQuestions(questions, category, difficulty) {
    try {
      const { results } = await fetch(
        `https://opentdb.com/api.php?amount=${questions}&category=${category}&difficulty=${difficulty}`
      ).then((r) => r.json());
      console.log(results);
      return [null, results];
    } catch (e) {
      return [e.message, []];
    }
  },
};
