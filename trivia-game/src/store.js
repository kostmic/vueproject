import Vuex from "vuex";
import Vue from "vue";
import { TriviaAPI } from "@/api/triviaAPI";
import { userAPI } from "@/api/userAPI";
import vSelect from "vue-select";

Vue.use(Vuex);
Vue.component("v-select", vSelect);

export default new Vuex.Store({
  state: {
    categorysError: "",
    triviaError: "",
    playerError: "",
    trivia_categories: [],
    trivia_questions: [],
    searchText: "",
    playerName: "",
    player: {},
    questionAmount: 0,
    difficulty: "",
    categoryId: "",
  },
  mutations: {
    setCategoryError: (state, payload) => {
      state.categorysError = payload;
    },
    setCategories: (state, payload) => {
      state.trivia_categories = payload;
    },
    setSearchFilterText: (state, payload) => {
      state.searchText = payload;
    },
    setPlayerName: (state, payload) => {
      state.playerName = payload;
    },
    setPlayer: (state, payload) => {
      state.player = payload;
    },
    setPlayerError: (state, payload) => {
      state.triviaError = payload;
    },
    setQuestionAmount: (state, payload) => {
      state.questionAmount = payload;
    },
    setDifficulty: (state, payload) => {
      state.difficulty = payload;
    },
    setCategoryId: (state, payload) => {
      state.categoryId = payload;
    },
    setTriviaQuestions: (state, payload) => {
      state.trivia_questions = payload;
    },
    setTriviaError: (state, payload) => {
      state.triviaError = payload;
    },
  },
  actions: {
    async fetchCategorys({ commit, state }) {
      if (state.trivia_categories.length !== 0)
        return new Promise((resolve) => resolve());

      const [error, trivia_categories] = await TriviaAPI.getCategory();
      commit("setCategoryError", error);
      commit("setCategories", trivia_categories);
    },

    async fetchTriviaQuestions({ commit, state }) {
      new Promise((resolve) => resolve());
      const [error, results] = await TriviaAPI.getTriviaQuestions(
        state.questionAmount,
        state.categoryId,
        state.difficulty
      );
      commit("setTriviaError", error);
      commit("setTriviaQuestions", results);
    },
    async fetchPlayer({ commit, state }) {
      const [error, results] = await userAPI.getPlayer(
        state.playerName
      );
      commit("setPlayerError", error);
      commit("setPlayer", results[0]);
    },
    async createPlayer({ commit, state }) {
      const [error, results] = await userAPI.createPlayer(
        state.playerName
      );
      commit("setPlayerError", error);
      commit("setPlayer", results[0]);
    },
    savedDifficulty({ commit }, difficultyLevel) {
      commit("setDifficulty", difficultyLevel);
    },
    savedQuestionAmount({ commit }, questionAmount) {
      commit("setQuestionAmount", questionAmount);
    },
    savedPlayerName({ commit }, playerName) {
      commit("setPlayerName", playerName);
    },
  },
  getters: {
    getQuestionAmount: (state) => {
      return state.questionAmount;
    },
    getPlayerName: (state) => {
      return state.playerName;
    },
    getPlayer: (state) => {
      return state.player;
    },
    getDifficulty: (state) => {
      return state.difficulty;
    },
    getTriviaQuestions: (state) => {
      return state.trivia_questions;
    },
  },
});
