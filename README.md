# VueProject - Quiz Game 📚

## Description

This is is an unfinished quiz game.
The site lets you enter a username that is stored for later, keeping track of your highscore.
You will be able to choose between multiple categories, difficulties and number of questions you want in your quiz.

## Heroku deployment
Michael: https://dry-island-67716.herokuapp.com/

Kristian: https://mysterious-ravine-02804.herokuapp.com/
## Getting started

### Installing

If you don't have Vue installed, run:

- npm install vue

To install Vue-select run:

- npm install vue-select

### Executing program

Navigate to triviaAPI.js:

` trivia-game -> src -> api -> triviaAPI.js`

Replace the apiKey and urlHeroku with the respective api-key and heroku URL:

```
const apiKey = 'ENTER API KEY HERE'
const urlHeroku = "ENTER URL HERE"
```

## Authors

Michael Kosther
@kostmic

Kristian Garberg
@Sirtakin
